'use strict';
interface ITemplate {
    setMessage(message : String);
    sendSMS(from : String, twilioAccountSid : String, twilioAuthToken : String);
    sendEmail(sparkPostApiKey : String, from : String, subjectLine: String);
}

export = ITemplate;