# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Creating quick Human Resources response, i.e. Acceptance/Rejection emails and texts.

### Example ###
```javascript
// Create recipient
var recipient = {
	name : 'John Doe',
	number : '+1212XXXXXXX',
	email : 'XXX@yahoo.com'
};

// Create letter
var reject = new Template(recipient, 'John Manager', 'XX Company', 
(new Date()).toLocaleDateString('en-US'), 'Sr. Java Developer');

// Send via SMS
reject.sendSMS('+1917XXXXXXX','<YOUR TWILIO SID>','<YOUR TWILIO TOKEN>');

// Send via Email
reject.sendEmail('<YOUR SPARKPOST API KEY>','john.manager@yahoo.com');
```

### Who do I talk to? ###

Hanjun Chen
hanjun@laposte.net