'use strict';
var SparkPost = require('sparkpost');
var Twilio = require('twilio');
/**
 * @prop {String} positionName - The name of the position applied to
 * @prop {String} message - message to be sent to the recipient
 * @prop {{}} recipient - recipient of the letter
 * @method setMessage - set custom message
 * 
 */
class Template {
    /**
     * 
     * @param {{}} recipient 
     *      - An Object contains: name, number, email
     * @param {String} managerName 
     * @param {String} companyName 
     * @param {Date | String} date 
     * @param {String} positionName 
     */
    constructor(recipient,managerName,companyName,date,positionName){
        this.message = `
            Dear ${recipient.name}:

            I am sorry to inform you that you have not been selected to the ${positionName} position.

            Sincerely,
            ${managerName}
            ${companyName}
            ${date}
        `;

        this.positionName = positionName;
        this.recipient = recipient;
    }

    /** @param {String} message - A new message that overrides the old one */
    setMessage (message){
        this.message = message;
    }

    /**
     * 
     * @param {String} from - the number that sends out the rejection letter
     * @param {String} twilioAccountSid - Twilio account sid
     * @param {String} twilioAuthToken - Twilio accoount token
     */
    sendSMS(from,twilioAccountSid, twilioAuthToken){
        var twilioClient = Twilio(twilioAccountSid, twilioAuthToken);
        twilioClient.messages.create({
                body: this.message,
                to: this.recipient.number,
                from: from
            },(err, message) => {
                console.log(message.sid);
            })
            .then((data)=>{
                console.log('Text message sent');
            });
    }

    /**
     * 
     * @param {String} sparkPostApiKey - SparkPost API Key
     * @param {String} from - The 's email address'
     * @param {String} subjectLine - Subject line of the email
     */
    sendEmail(sparkPostApiKey, from, subjectLine){
        var subject = (subjectLine===undefined) ? 
            `Application Status of ${this.positionName}` : 'Application Status';
        
        var sparkPostClient = new SparkPost(sparkPostApiKey);
        sparkPostClient.transmissions.send({
                content:{
                    from:from,
                    subject : subject,
                    html: `<html><body><p>
                    ${this.message}
                    </p></body></html>`
                },
                recipients: [
                    {address: this.recipient.email}
                ]
            })
            .then((data)=>{
                console.log('Email has been sent');
            }).catch((err)=>{
                console.log(err);
            });
    }
}

module.exports = Template;

